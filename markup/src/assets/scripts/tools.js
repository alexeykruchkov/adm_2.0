// mobile menu
$('.mobile-menu-trigger').on("click", function(){
  $(this).toggleClass('active');
  $('.hdr-nav').toggleClass('active');
  $('body').toggleClass('overflow');
});

$('.hdr-nav__item a').on("click", function(){
  $('body').removeClass('overflow');
  $('.hdr-nav').removeClass('active');
  $('.mobile-menu-trigger').removeClass('active');
});

//form errror example
$('.form .input-label.test').removeClass('alert');
$('.form-btn').on("click", function(){
  $('.form .input-label.test').addClass('alert');
});